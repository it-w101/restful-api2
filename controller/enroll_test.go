package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddEnroll(t *testing.T) {
	url := "http://localhost:8080/enroll"
	var jsonStr = []byte(`{"stdid":1, "cid":"3123"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"status": "enrolled"}`
	assert.JSONEq(t, expResp, string(body))
}

// get enroll
func TestGetEnroll(t *testing.T) {
	c := http.Client{}
	r, _ := c.Get("http://localhost:8080/enroll/1/3123")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp := `{
		"stdid": 1,
		"cid": "3123",
		"date": "2024-05-12T14:50:09Z"
	  }`
	assert.JSONEq(t, expResp, string(body))
}

// delete enroll
func TestDeleteEnroll(t *testing.T) {
	url := "http://localhost:8080/enroll/1/ww3"
	req, _ := http.NewRequest("DELETE", url, nil)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status":"deleted"}`
	assert.JSONEq(t, expResp, string(body))
}

// not found
func TestCoursetNotFound(t *testing.T) {
	assert := assert.New(t)
	c := http.Client{}
	r, _ := c.Get("http://localhost:8080/enroll/12/1")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"No such enrollment"}`
	assert.JSONEq(expResp, string(body))
}
