package controller

import (
	"bytes"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

// add course test
func TestAddCourse(t *testing.T) {
	url := "http://localhost:8080/course"
	var jsonStr = []byte(`{"courseid": "5","coursename":"eng"}`)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusCreated, resp.StatusCode)
	expResp := `{"status": "course added"}`
	assert.JSONEq(t, expResp, string(body))

}

// get course test
func TestGetCourse(t *testing.T) {
	s := http.Client{}
	r, _ := s.Get("http://localhost:8080/course/4")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(t, http.StatusOK, r.StatusCode)
	expResp := `{
		"courseid": "4",
		"coursename": "math"
	  }`
	assert.JSONEq(t, expResp, string(body))
}

// delete
func TestDeleteCourse(t *testing.T) {
	url := "http://localhost:8080/course/4"
	req, _ := http.NewRequest("DELETE", url, nil)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, _ := io.ReadAll(resp.Body)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	expResp := `{"status":"deleted"}`
	assert.JSONEq(t, expResp, string(body))

}

//not found

func TestCourseNotFound(t *testing.T) {
	assert := assert.New(t)
	c := http.Client{}
	r, _ := c.Get("http://localhost:8080/course/1002")
	body, _ := io.ReadAll(r.Body)
	assert.Equal(http.StatusNotFound, r.StatusCode)
	expResp := `{"error":"status not found"}`
	assert.JSONEq(expResp, string(body))
}
