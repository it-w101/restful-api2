CREATE DATABASE my_db;

CREATE TABLE student (
StdId int NOT NULL,
FirstName varchar(45) NOT NULL,
LastName varchar(45) DEFAULT NULL,
Email varchar(45) NOT NULL,
PRIMARY KEY (StdId),
UNIQUE (Email)
)

CREATE TABLE course (
cid varchar(32) NOT NULL,
coursename varchar(45) NOT NULL,
PRIMARY KEY (cid)
)

CREATE TABLE admin (
FirstName varchar(45) NOT NULL,
LastName varchar(45) DEFAULT NULL,
Email varchar(45) NOT NULL,
Password varchar(45) NOT NULL,
PRIMARY KEY (Email)
)


